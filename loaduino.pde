
#include <Servo.h> 

Servo myservo;  // create servo object to control a servo 
                // a maximum of eight servo objects can be created 

int dial_pos = 0; // variable to store the servo position 
int led_pin = 13;
int difference = 0;
 
unsigned char value1 = 0, value2 = 0;
int value = 0;
 
void setup()
{
  digitalWrite(led_pin, LOW);
  Serial.begin(9600); //set serial to 9600 baud rate
  myservo.attach(9);  // attaches the servo on pin 9 to the servo object 
  myservo.write(dial_pos);
} 
 
 
void loop() 
{
   while(Serial.available() <= 0);
   value1 = Serial.read();
   
   while(Serial.available() <= 0);
   value2 = Serial.read();
   
   // we're looking to make two bytes into a 16 bit int here
   value = value2;
   value = value << 8;
   value = value | value1;
    
   Serial.println("servo value is:"); // some debugging
   Serial.println(value);
   
   // as long as we're above zero, drop off slowly when there's no
   // activity
   if ((value == dial_pos) && (dial_pos > 0))
     value--;
   difference = abs(dial_pos - value);
   dial_pos = value;
   
   // if there's a big change, blinkenlight
   if (difference > 5) {
     digitalWrite(led_pin, HIGH);
     delay(10 * difference);
     digitalWrite(led_pin, LOW); 
     Serial.flush();
   }
   
   for (int i = dial_pos - difference; i <= dial_pos; i++) {
     myservo.write(i);
     delay(10);
   }
}
